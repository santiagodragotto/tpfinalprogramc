<?php
    function Conectarse(){
        if (!($link = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)))  
	    {  
	        echo "Error conectando a la base de datos.";
	        exit();
	    }
        return $link;
    }

    function ExecuteSelectUnique($query, $parameters)
    {
        $conn = Conectarse();
        
        $query = BindParameters($query, $parameters);
        $result = mysqli_fetch_array(mysqli_query($conn, $query));
        mysqli_close($conn);
        return $result;
    }

    function ExecuteSelectList($query, $parameters)
    {
        $conn = Conectarse();
        
        $query = BindParameters($query, $parameters);
        $result = mysqli_fetch_all(mysqli_query($conn, $query));
        mysqli_close($conn);
        return $result;
    }

    function ExecuteQuery($query, $parameters)
    {
        $conn = Conectarse();
        
        $query = BindParameters($query, $parameters);
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        return $result;
    }

    function ExecuteQueryInsert($query, $parameters)
    {
        $conn = Conectarse();
        
        $query = BindParameters($query, $parameters);
        $result = mysqli_query($conn, $query);
        $result = mysqli_insert_id($conn);
        mysqli_close($conn);
        return $result;
    }

    function BindParameters($query, $parameters = array())
    {
        foreach($parameters as $parameterName => $value)
        {                
            if($value == ''){
                $query = str_replace(":".$parameterName, "null", $query);
            } else {
                $query = str_replace(":".$parameterName, "'$value'", $query);
            }
            
        }
        return $query;
    }

?>