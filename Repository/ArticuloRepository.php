<?php
    include("Conexion.php");

    use Models\Articulo as Articulo;

    function CrearArticulo(Articulo $articulo){
        $query = "INSERT INTO Articulo (Descripcion, Precio, RubroID)
        VALUES (:descripcion, :precio, :rubroID);";

        $parameters["descripcion"] = $articulo->getDescripcion();
        $parameters["precio"] = $articulo->getPrecio();
        $parameters["rubroID"] = $articulo->getRubroID();

        $result = ExecuteQueryInsert($query, $parameters);

        $query = "INSERT INTO Publicacion (Fecha, UsuarioID, ArticuloID)
        VALUES (NOW(), :usuarioID, :articuloID);";

        $parameters["usuarioID"] = $_SESSION["User"]["UsuarioID"];
        $parameters["articuloID"] = $result;
        $parameters["rubroID"] = $articulo->getRubroID();

        $result1 = ExecuteQueryInsert($query, $parameters);

        return $result;
    }

    function AgregarImagenArticulo($articuloId, $path) {
        $query = "Update Articulo SET Imagen = :imagen WHERE ArticuloID = :articuloId;";

        $parameters["imagen"] = $path;
        $parameters["articuloId"] = $articuloId;

        $result = ExecuteQuery($query, $parameters);

        return $result;
    }

    function GetArticulos($descripcion, $rubros, $precioDesde, $precioHasta) {
        $consultaRubro = "";
        if(!empty($rubros)){
            $consultaRubro = $consultaRubro."a.RubroID = '".$rubros[0]."'";
        }
        for($i = 1; $i < count($rubros); $i++){
            $consultaRubro = $consultaRubro." OR a.RubroID = '".$rubros[$i]."'";
        }

        $consultaPrecio = "";
        if(!empty($precioDesde) && !empty($precioHasta)) {
            $consultaPrecio = $consultaPrecio."a.Precio >= ".$precioDesde." AND a.Precio <= ".$precioHasta."";
        } else {
            if(!empty($precioDesde)){
                $consultaPrecio = $consultaPrecio."a.Precio >= ".$precioDesde."";
            }
            if(!empty($precioHasta)){
                $consultaPrecio = $consultaPrecio."a.Precio <= ".$precioHasta."";
            }
        }

        $query = "SELECT a.ArticuloId, a.Descripcion, a.Precio, a.Imagen, r.Descripcion FROM Articulo a JOIN Rubro r ON (a.RubroID = r.RubroID) WHERE a.Descripcion LIKE '%".$descripcion."%'";
        if($consultaRubro != ""){
            $query = $query." AND (".$consultaRubro.") ";
        }
        if($consultaPrecio != ""){
            $query = $query." AND ".$consultaPrecio." ";
        }
        $parameters = array();
        $result = ExecuteSelectList($query, $parameters);

        return $result;
    }

?>