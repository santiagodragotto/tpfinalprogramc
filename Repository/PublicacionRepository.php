<?php
    include("Conexion.php");

    use Models\Articulo as Articulo;

    function GetPublicaciones($descripcion, $rubros, $precioDesde, $precioHasta) {
        $consultaRubro = "";
        if(!empty($rubros)){
            $consultaRubro = $consultaRubro."a.RubroID = '".$rubros[0]."'";
        }
        for($i = 1; $i < count($rubros); $i++){
            $consultaRubro = $consultaRubro." OR a.RubroID = '".$rubros[$i]."'";
        }

        $consultaPrecio = "";
        if(!empty($precioDesde) && !empty($precioHasta)) {
            $consultaPrecio = $consultaPrecio."a.Precio >= ".$precioDesde." AND a.Precio <= ".$precioHasta."";
        } else {
            if(!empty($precioDesde)){
                $consultaPrecio = $consultaPrecio."a.Precio >= ".$precioDesde."";
            }
            if(!empty($precioHasta)){
                $consultaPrecio = $consultaPrecio."a.Precio <= ".$precioHasta."";
            }
        }

        $query = "SELECT p.PublicacionID, p.Fecha, p.ArticuloID, a.Descripcion, a.Precio, a.Imagen, r.Descripcion, u.NombreUsuario FROM Publicacion p
            JOIN Articulo a ON (p.ArticuloID = a.ArticuloID) 
            JOIN Rubro r ON (a.RubroID = r.RubroID)
            JOIN Usuario u ON (p.UsuarioID = u.UsuarioID)
            WHERE a.Descripcion LIKE '%".$descripcion."%'";
        if($consultaRubro != ""){
            $query = $query." AND (".$consultaRubro.") ";
        }
        if($consultaPrecio != ""){
            $query = $query." AND ".$consultaPrecio." ";
        }
        $parameters = array();
        $result = ExecuteSelectList($query, $parameters);

        return $result;
    }

    function GetPublicacionesByUsuario($usuarioId){
        $query = "SELECT p.PublicacionID, p.Fecha, p.ArticuloID, a.Descripcion, a.Precio, a.Imagen, r.Descripcion, p.UsuarioID FROM Publicacion p 
            JOIN Articulo a ON (p.ArticuloID = a.ArticuloID) 
            JOIN Rubro r ON (a.RubroID = r.RubroID)
            WHERE p.UsuarioID = '".$usuarioId."'";
        $parameters = array();
        $result = ExecuteSelectList($query, $parameters);

        return $result;
    }

    function GetPublicacion($publicacionId) {
        $query = "SELECT p.PublicacionID, p.Fecha, p.ArticuloID, a.Descripcion, a.Precio, a.Imagen, r.Descripcion, u.NombreUsuario FROM Publicacion p 
            JOIN Articulo a ON (p.ArticuloID = a.ArticuloID) 
            JOIN Rubro r ON (a.RubroID = r.RubroID)
            JOIN Usuario u ON (p.UsuarioID = u.UsuarioID)
            WHERE p.PublicacionID = '".$publicacionId."'";
        $parameters = array();
        $result = ExecuteSelectList($query, $parameters);

        return $result;
    }

    function ComentarPublicacion($publicacionId, $comentario){
        $query = "INSERT INTO Consulta (Fecha, PublicacionID, Comentario, UsuarioID)
        VALUES (NOW(), :publicacionID, :comentario, :usuarioID);";

        $parameters["usuarioID"] = $_SESSION["User"]["UsuarioID"];
        $parameters["publicacionID"] = $publicacionId;
        $parameters["comentario"] = $comentario;

        $result = ExecuteQueryInsert($query, $parameters);

        return $result;
    }

    function GetConsultasByPublicacion($publicacionId){
        $query = "SELECT c.Comentario, c.Fecha, u.NombreUsuario, c.PublicacionID FROM Consulta c 
            JOIN Usuario u ON (c.UsuarioID = u.UsuarioID)
            WHERE c.PublicacionID = '".$publicacionId."'
            ORDER BY c.Fecha DESC";
        $parameters = array();
        $result = ExecuteSelectList($query, $parameters);

        return $result;
    }

    function GetConsultaByID($consultaID){
        $query = "SELECT c.Comentario, c.Fecha, u.NombreUsuario, c.PublicacionID FROM Consulta c 
            JOIN Usuario u ON (c.UsuarioID = u.UsuarioID)
            WHERE c.ConsultaID = '".$consultaID."'
            ORDER BY c.Fecha DESC";
        $parameters = array();
        $result = ExecuteSelectList($query, $parameters);

        return $result;
    }
?>