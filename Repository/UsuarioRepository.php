<?php
    include("Conexion.php");

    use Models\Usuario as Usuario;

    function GetUsuarioByEmail($email) {
        $query = "SELECT * FROM Usuario WHERE Email = :email limit 1;";

        $parameters["email"] = $email;
        $result = ExecuteSelectUnique($query, $parameters);

        return $result;
    }

    function GetUsuarioByNombreUsuario($userName) {
        $query = "SELECT * FROM Usuario WHERE NombreUsuario = :userName limit 1;";

        $parameters["userName"] = $userName;
        $result = ExecuteSelectUnique($query, $parameters);

        return $result;
    }


    function ValidarUsuario($email, $userName) {
        $errors = array();
        $result = GetUsuarioByEmail($email);
        if(!empty($result)){
            array_push($errors, "El mail ya esta en uso");
        }
        $result = GetUsuarioByNombreUsuario($userName);
        if(!empty($result)){
            array_push($errors, "El nombre de usuario ya esta en uso");
        }
        return $errors;
    }

    function CrearUsuario(Usuario $user){
        $query = "INSERT INTO " . Usuario . " (Apellido, Nombre, Email, NombreUsuario, Password, Sexo, FechaNacimiento, Telefono)
        VALUES (:apellido, :nombre1, :email, :nombreUsuario, :password, :sexo, :fechaNacimiento, :telefono);";

        $parameters["apellido"] = $user->getApellido();
        $parameters["nombre1"] = $user->getNombre(); //le agrego el 1 porque pisaba con nombreUsuario, revisar
        $parameters["email"] = $user->getEmail();
        $parameters["nombreUsuario"] = $user->getNombreUsuario();
        $parameters["password"] = $user->getPassword();
        $parameters["sexo"] = $user->getSexo();
        $parameters["fechaNacimiento"] = $user->getFechaNacimiento();
        $parameters["telefono"] = $user->getTelefono();

        $result = ExecuteQuery($query, $parameters);

        return $result;
    }

    function UpdateUsuario(Usuario $user){
        $query = "Update Usuario SET Apellido = :apellido, Nombre = :nombre1, Email = :email, NombreUsuario = :nombreUsuario, Password = :password,
            Sexo = :sexo, FechaNacimiento = :fechaNacimiento, Telefono = :telefono WHERE UsuarioID = :usuarioId";

        $parameters["usuarioId"] = $user->getUsuarioID();
        $parameters["apellido"] = $user->getApellido();
        $parameters["nombre1"] = $user->getNombre(); //le agrego el 1 porque pisaba con nombreUsuario, revisar
        $parameters["email"] = $user->getEmail();
        $parameters["nombreUsuario"] = $user->getNombreUsuario();
        $parameters["password"] = $user->getPassword();
        $parameters["sexo"] = $user->getSexo();
        $parameters["fechaNacimiento"] = $user->getFechaNacimiento();
        $parameters["telefono"] = $user->getTelefono();

        $result = ExecuteQuery($query, $parameters);

        return $result;
    }
    
    function GetUsuario($userName, $password) {
        $query = "SELECT * FROM Usuario WHERE NombreUsuario = :userName && Password = :password limit 1;";

        $parameters["userName"] = $userName;
        $parameters["password"] = $password;
        $result = ExecuteSelectUnique($query, $parameters);
        
        return $result;
    }

?>