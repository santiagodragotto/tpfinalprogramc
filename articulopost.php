<?php
    include("Views/header.php");
    include("Repository/ArticuloRepository.php");
    use Models\Articulo as Articulo;

    if($_POST) {
        $articulo = new Articulo();
        $articulo->setDescripcion($_POST["descripcion"]);
        $articulo->setPrecio($_POST["precio"]);
        $articulo->setRubroID($_POST["rubro"]);

        $error = false;

        $result = CrearArticulo($articulo);
        if($result != 0){
            $_SESSION["message"] = "Articulo creado correctamente";
            $_SESSION["articuloId"] = $result;
            
            header("Location: articuloimg.php");
            stop;
        } else {
            $_SESSION["errors"] = ["Error al crear el articulo"];
            $error = true;
        }

        if($error){
            $_SESSION["inputDescripcion"] = $_POST["descripcion"];
            $_SESSION["inputPrecio"] = $_POST["precio"];
            $_SESSION["inputRubroId"] = $_POST["rubro"];
            
            header("Location: articulo.php");
            stop;
        }

    } else {
        header("Location: articulo.php");
        stop;
    }

?>