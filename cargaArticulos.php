<?php
    include("Views/header.php");
    include("Repository/PublicacionRepository.php");

    if($_POST) {
        $publicaciones = GetPublicaciones($_POST["descripcion"], json_decode($_POST["rubros"]), $_POST["precioDesde"], $_POST["precioHasta"]);
        $result = "";
        foreach ($publicaciones as $publicacion){
            $img = "";
            if($publicacion[5] != ""){
                $img = "<img src='Views/img/".$publicacion[5]."' class='w3-left w3-circle' width='150' height='150'>";
            }

            $result = $result."
            <form action='".FRONT_ROOT."verpublicacion.php' method='POST' class='login-form'>
            <input type='hidden' name='publicacionId' value='".$publicacion[0]."'/>
            <div class='w3-card-4' style='padding-bottom:10px'>

                <header class='w3-container w3-light-grey'>
                <h3>".$publicacion[3]."</h3>
                </header>
                
                <div class='w3-container'>
                <p>Precio: ".$publicacion[4]."</p>
                <hr>
                ".$img."
                <p>Rubro: ".$publicacion[6]."</p>
                <p>Usuario: ".$publicacion[7]."</p>
                <p>Publicado desde: ".$publicacion[1]."</p>
                </div>
                
                <button type='submit' class='w3-btn-block w3-dark-grey'>+ Ver</button>
            
            </div>
            <hr>
            </form>";
        }

        echo $result;
    } else {
        header("Location: articulo.php");
        stop;
    }
?>