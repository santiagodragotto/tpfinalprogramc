<?php
include("Views/header.php");
include(VIEWS_PATH."nav.php");
include("Repository/RubroRepository.php");

if(!isset($_SESSION["isLogged"])){
    $_SESSION["message"] = "Debe iniciar sesión para agregar artículos.";
    header("Location: login.php");
    stop;
}

?>


<link rel="stylesheet" href="<?= CSS_PATH ?>producto.css">
<script src="<?= JS_PATH ?>producto.js" crossorigin="anonymous"></script>

<main class="d-flex align-items-center justify-content-center height-100" >
    <div class="login-content">
        <div class="login-logo text-center">
                <img src="<?= IMG_PATH ?>logocarro.png" alt="Productos Logo" width="114px" height="116px"/>
                <h2 id="login-title">Agregar imagen al artículo</h2>
        </div>
        
        <form action="<?= FRONT_ROOT ?>articuloimgpost.php" method="POST" class="login-form" enctype="multipart/form-data">
                <?php if(isset($_SESSION["message"])) {?>
                    <strong style="color:red;"><?=$_SESSION["message"]?></strong>
                <?php } unset($_SESSION["message"])?>
                <!-- Error en registro -->
                <?php
                    if(isset($_SESSION["errors"])){
                        foreach($_SESSION["errors"] as $error){
                            echo "<strong style='color:red;'>$error</strong><br/>";
                        }
                    }
                    unset($_SESSION["errors"]);
                ?>

                <div class="form-content">
                    <input type="hidden" name="articuloId"
                    <?php
                        if(isset($_SESSION["articuloId"])){
                            echo "value='".$_SESSION["articuloId"]."'";
                        }
                        unset($_SESSION["articuloId"]);
                    ?>
                    />
                    <div class="form-group">
                        <i class="fas fa-pen form-icon"></i>
                        <input type="file" name="image" class="form-control form-control-md login-input">
                    </div>
                </div>
                <button class="login-btn" type="submit">Guardar imagen</button>
        </form>
    </div>
</main>

<?php
include(VIEWS_PATH."footer.php");
?>