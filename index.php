<?php
include("Views/header.php");
include(VIEWS_PATH."nav.php");
include("Repository/RubroRepository.php");
?>

<script src="<?= JS_PATH ?>index.js" crossorigin="anonymous"></script>

<div class="container">
    <!-- Inicio Index -->
    <!-- Listado de productos -->
    <div id="box" class="row justify-content-center" style="background-color: #242424;">
        <!-- Inicio Listado de Productos -->
        <div class="col-md-12" style="text-align:center">
            <h1 class="basic-font">Artículos</h1>
        </div>
        <div class="col-md-3">
            <h1 class="basic-font">Filtros</h1>
            <div class="form-content-filtro">
                <div class="form-group">
                    <input type="text" id="filtroDescripcion" class="form-control form-control-md login-input" placeholder="Descripcion" />
                </div>
                <div class="form-group">
                    <p class="basic-font" style="font-size:25px">Rubros<p>
                    <?php
                        $rubros = GetRubros();
                        foreach ($rubros as $rubro){
                            echo "<label class='label'><input type='checkbox' id='rubro' value='".$rubro[0]."' class='rubros'> ".$rubro[1]."</label><br/>";
                        }
                    ?>
                </div>
                <div class="form-group">
                    <input type="number" id="precioDesde" class="form-control form-control-md login-input" min=0 placeholder="Precio desde">
                </div>
                <div class="form-group">
                    <input type="number" id="precioHasta" class="form-control form-control-md login-input" min=0 placeholder="Precio hasta">
                </div>
            </div>
            <button class="login-btn" type="submit" onclick="cargarProductos();">Buscar</button>
        </div>
        <div class="col-md-9" id="resultado">
            Productos
        </div>
    </div>
</div>

<?php
include(VIEWS_PATH."footer.php");
?>


<script>
    $(document).ready(function (){
        cargarProductos();
    });

    function cargarProductos(){
        var rubros = $(".rubros");
        var arrRubros = [];
        
        for(var i = 0; i < rubros.length; i++) {
            if ($(rubros[i]).prop("checked")) {
                arrRubros.push($(rubros[i]).val())
            }
        }

        var parametros = {
            "descripcion": $("#filtroDescripcion").val(),
            "precioDesde": $("#precioDesde").val(),
            "precioHasta": $("#precioHasta").val(),
            "rubros": JSON.stringify(arrRubros)
        };
        $.ajax({
            data:  parametros,
            url:   'cargaArticulos.php',
            type:  'post',
            beforeSend: function () {
                $("#resultado").html("Procesando, espere por favor...");
            },
            success:  function (response) {
                $("#resultado").html(response);
            }
        });
    }

</script>