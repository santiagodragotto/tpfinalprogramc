<?php
include("Views/header.php");
include(VIEWS_PATH."nav.php");

if(isset($_SESSION["isLogged"])){
    header("Location: index.php");
}

?>

<link rel="stylesheet" href="<?= CSS_PATH ?>/login.css">

<main class="d-flex align-items-center justify-content-center height-100" >
    <div class="login-content">
        <div class="login-logo text-center">
                <img src="<?= IMG_PATH ?>/logocarro.png" alt="Productos Logo" width="114px" height="116px"/>
                <h2 id="login-title">Iniciar sesión</h2>
        </div>

        
        <form action="<?= FRONT_ROOT ?>loginpost.php" method="POST" class="login-form">
                <?php if(isset($_SESSION["message"])) {?>
                    <strong style="color:red;"><?=$_SESSION["message"]?></strong>
                <?php } unset($_SESSION["message"])?>
                <div class="form-content">
                    <div class="form-group">
                        <i class="fas fa-user form-icon"></i>
                        <input type="text" name="username" class="form-control form-control-md login-input" placeholder="Nombre de usuario" required
                        <?php
                            if(isset($_SESSION["inputNombreUsuario"])){
                                echo "value='".$_SESSION["inputNombreUsuario"]."'";
                            }
                            unset($_SESSION["inputNombreUsuario"]);
                        ?>
                        >
                    </div>
                    <div class="form-group">
                        <i class="fas fa-lock form-icon"></i>
                        <input type="password" name="password" class="form-control form-control-md login-input" placeholder="Contraseña" required
                        <?php
                            if(isset($_SESSION["inputPassword"])){
                                echo "value='".$_SESSION["inputPassword"]."'";
                            }
                            unset($_SESSION["inputPassword"]);
                        ?>
                        >
                    </div>
                </div>
                <button class="login-btn" type="submit">Iniciar Sesión</button>
        </form>
    </div>
</main>


<?php
include(VIEWS_PATH."footer.php");
?>