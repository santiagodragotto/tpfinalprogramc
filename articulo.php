<?php
include("Views/header.php");
include(VIEWS_PATH."nav.php");
include("Repository/RubroRepository.php");

if(!isset($_SESSION["isLogged"])){
    $_SESSION["message"] = "Debe iniciar sesión para agregar artículos.";
    header("Location: login.php");
    stop;
}

?>


<link rel="stylesheet" href="<?= CSS_PATH ?>producto.css">
<script src="<?= JS_PATH ?>producto.js" crossorigin="anonymous"></script>

<main class="d-flex align-items-center justify-content-center height-100" >
    <div class="login-content">
        <div class="login-logo text-center">
                <img src="<?= IMG_PATH ?>logocarro.png" alt="Productos Logo" width="114px" height="116px"/>
                <h2 id="login-title">Agregar Artículo</h2>
        </div>
        
        <form action="<?= FRONT_ROOT ?>articulopost.php" method="POST" class="login-form">
                <!-- Error en registro -->
                <?php
                    if(isset($_SESSION["errors"])){
                        foreach($_SESSION["errors"] as $error){
                            echo "<strong style='color:red;'>$error</strong><br/>";
                        }
                    }
                    unset($_SESSION["errors"]);
                ?>

                <div class="form-content">
                    <div class="form-group">
                        <i class="fas fa-pen form-icon"></i>
                        <input type="hidden" name="articuloId" />
                        <input type="text" name="descripcion" class="form-control form-control-md login-input" placeholder="Descripcion" required
                        <?php
                            if(isset($_SESSION["inputDescripcion"])){
                                echo "value='".$_SESSION["inputDescripcion"]."'";
                            }
                            unset($_SESSION["inputDescripcion"]);
                        ?>
                        >
                    </div>
                    <div class="form-group">
                        <i class="fas fa-dollar-sign form-icon"></i>
                        <input type="number" name="precio" class="form-control form-control-md login-input" min=0 placeholder="Precio" required
                        <?php
                            if(isset($_SESSION["inputPrecio"])){
                                echo "value='".$_SESSION["inputPrecio"]."'";
                            }
                            unset($_SESSION["inputPrecio"]);
                        ?>
                        >
                    </div>
                    <div class="form-group">
                        <i class="fas fa-pen form-icon"></i>
                        <select name="rubro" class="form-control form-control-md login-input" required>
                            <option value="">Seleccione rubro...</option>
                            <?php
                                $rubros = GetRubros();
                                foreach ($rubros as $rubro){
                                    echo "<option value='".$rubro[0]."'>".$rubro[1]."</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <button class="login-btn" type="submit">Continuar</button>
        </form>
    </div>
</main>

<?php
include(VIEWS_PATH."footer.php");
?>