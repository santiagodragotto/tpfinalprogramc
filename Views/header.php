<!doctype html>
<html lang="en">
<head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

     <?php
        require "Config/Autoload.php";
        require "Config/Config.php";

        use Config\Autoload as Autoload;
            
        Autoload::start();

        session_start();
     ?>

     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="http://www.w3ii.com/lib/w3.css"> 
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
     <script src="https://kit.fontawesome.com/00d5e0a4ab.js" crossorigin="anonymous"></script>
     <link rel="stylesheet" href="<?= CSS_PATH ?>index.css">
     <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
     <link rel="icon" href="<?= IMG_PATH ?>logocarro.png" type="image/x-icon">
     <title>TP Programacion C</title>

</head>
<body>