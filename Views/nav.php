<link rel="stylesheet" href="<?= CSS_PATH ?>nav.css">

<!-- NavBar -->
<nav class="navbar navbar-expand-lg navbar-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarToggler">
        <!-- Titulo con logo -->
        <a class="navbar-brand " href="<?= FRONT_ROOT ?>">
            <img src="<?= IMG_PATH ?>logocarro.png" width="50" height="50">
            Articulos
        </a>

        <!--Opcion Perfil -->
        <?php
            if(isset($_SESSION["isLogged"])){  

                echo '<ul class="nav navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" id="navbarDropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">';echo $_SESSION['User']['NombreUsuario'] . '</a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="';echo FRONT_ROOT. 'articulo.php"><i class="fas fa-user-alt"></i>&nbspAgregar Artículo</a>
                        <a class="dropdown-item" href="';echo FRONT_ROOT. 'perfil.php"><i class="fas fa-user-alt"></i>&nbspMi Perfil</a>
                        <a class="dropdown-item" href="';echo FRONT_ROOT. 'misarticulos.php"><i class="fas fa-ticket-alt"></i>&nbspMis Artículos</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="';echo FRONT_ROOT. 'cerrarsesion.php"><i class="fas fa-sign-out-alt"></i>&nbspCerrar Sesión</a>
                        </div>
                    </li>                     
                    </ul>';   
                
            } else {
                echo '<ul class="nav navbar-nav ml-auto">
                        <li class="nav-item">
                        <a class="nav-link" href="';echo FRONT_ROOT. 'login.php"><i class="fas fa-sign-in-alt"></i></i>&nbspIniciar sesión</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="';echo FRONT_ROOT. 'register.php"><i class="fas fa-user-plus"></i></i>&nbspRegistrarse</a>
                        </li>
                    </ul>';    
            }  
        ?>
    </div>
</nav>
