<?php
    include("Views/header.php");

    if(isset($_SESSION["isLogged"])){
        $user = $_SESSION["User"];
        
        $_SESSION["inputUsuarioId"] = $user["UsuarioID"];
        $_SESSION["inputApellido"] = $user["Apellido"];
        $_SESSION["inputNombre"] = $user["Nombre"];
        $_SESSION["inputEmail"] = $user["Email"];
        $_SESSION["inputNombreUsuario"] = $user["NombreUsuario"];
        $_SESSION["inputSexo"] = $user["Sexo"];
        $_SESSION["inputFechaNacimiento"] = $user["FechaNacimiento"];
        $_SESSION["inputTelefono"] = $user["Telefono"];

        header("Location: register.php");
        stop;
    } else {
        $_SESSION["message"] = "Debe iniciar sesión para entrar al perfil.";
        header("Location: login.php");
        stop;
    }
?>