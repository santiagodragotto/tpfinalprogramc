<?php
    include("Views/header.php");
    include("Repository/UsuarioRepository.php");
    use Models\Usuario as Usuario;

    if($_POST) {
        $user = new Usuario();
        $user->setApellido($_POST["apellido"]);
        $user->setNombre($_POST["nombre"]);
        $user->setEmail($_POST["email"]);
        $user->setNombreUsuario($_POST["username"]);
        $user->setPassword(md5($_POST["password"]));
        $user->setSexo($_POST["sexo"]);
        $user->setFechaNacimiento($_POST["fechaNacimiento"]);
        $user->setTelefono($_POST["telefono"]);

        $error = false;

        if($_POST["usuarioId"] == ""){
            $errors = ValidarUsuario($user->getEmail(), $user->getNombreUsuario());
        }
        if(empty($errors)) {
            if($_POST["usuarioId"] != "") { //update
                $user->setUsuarioID($_POST["usuarioId"]);
                $result = UpdateUsuario($user);
                if($result == true){
                    $result = GetUsuario($_POST["username"], md5($_POST["password"]));
                    if(!empty($result)){
                        $_SESSION["isLogged"] = true;
                        $_SESSION["User"] = $result;
                        header("Location: index.php");
                        stop;
                    }
                    header("Location: index.php");
                    stop;
                } else {
                    $_SESSION["errors"] = ["Error al guardar el usuario"];
                    $error = true;
                }
            } else { //create
                $result = CrearUsuario($user);
                if($result == true){
                    $_SESSION["message"] = "Usuario creado correctamente";
                    header("Location: login.php");
                    stop;
                } else {
                    $_SESSION["errors"] = ["Error al crear el usuario"];
                    $error = true;
                }
            }
            
        } else {
            $_SESSION["errors"] = $errors;
            $error = true;
        }

        if($error){
            if($_POST["usuarioId"] != ""){
                $_SESSION["inputUsuarioId"] = $_POST["usuarioId"];
            }
            $_SESSION["inputApellido"] = $_POST["apellido"];
            $_SESSION["inputNombre"] = $_POST["nombre"];
            $_SESSION["inputEmail"] = $_POST["email"];
            $_SESSION["inputNombreUsuario"] = $_POST["username"];
            $_SESSION["inputPassword"] = $_POST["password"];
            $_SESSION["inputConfirmPassword"] = $_POST["confirmpassword"];
            $_SESSION["inputSexo"] = $_POST["sexo"];
            $_SESSION["inputFechaNacimiento"] = $_POST["fechaNacimiento"];
            $_SESSION["inputTelefono"] = $_POST["telefono"];
            header("Location: register.php");
            stop;
        }

    } else {
        header("Location: register.php");
        stop;
    }

?>