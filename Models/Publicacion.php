<?php

namespace Models;

class Publicacion {

    private $publicacionID;
    private $fecha;
    private $usuarioID;
    private $articuloID;

    public function getPublicacionID()
    {
        return $this->publicacionID;
    }

    public function setPublicacionID($publicacionID)
    {
        $this->publicacionID = $publicacionID;

        return $this;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getUsuarioID()
    {
        return $this->usuarioID;
    }

    public function setUsuarioID($usuarioID)
    {
        $this->usuarioID = $usuarioID;

        return $this;
    }
 
    public function getArticuloID()
    {
        return $this->articuloID;
    }

    public function setArticuloID($articuloID)
    {
        $this->articuloID = $articuloID;

        return $this;
    }
    
}

?>