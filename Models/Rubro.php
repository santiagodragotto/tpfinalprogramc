<?php

namespace Models;

class Rubro {

    private $rubroID;
    private $descripcion;

    public function getRubroID()
    {
        return $this->rubroID;
    }

    public function setRubroID($rubroID)
    {
        $this->rubroID = $rubroID;

        return $this;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }
    
}

?>