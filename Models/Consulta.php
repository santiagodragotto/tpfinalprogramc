<?php

namespace Models;

class Consulta {

    private $consultaID;
    private $fecha;
    private $publicacionID;
    private $consultaPadreID;
    private $comentario;

    public function getConsultaID()
    {
        return $this->consultaID;
    }

    public function setConsultaID($consultaID)
    {
        $this->consultaID = $consultaID;

        return $this;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getPublicacionID()
    {
        return $this->publicacionID;
    }

    public function setPublicacionID($publicacionID)
    {
        $this->publicacionID = $publicacionID;

        return $this;
    }

    public function getConsultaPadreID()
    {
        return $this->consultaPadreID;
    }

    public function setConsultaPadreID($consultaPadreID)
    {
        $this->consultaPadreID = $consultaPadreID;

        return $this;
    }

    public function getComentario()
    {
        return $this->comentario;
    }

    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }
    
}

?>