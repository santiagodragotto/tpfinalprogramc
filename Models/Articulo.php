<?php

namespace Models;

class Articulo {

    private $articuloID;
    private $descripcion;
    private $precio;
    private $imagen;
    private $rubroID;

    public function getArticuloID()
    {
        return $this->articuloID;
    }

    public function setArticuloID($articuloID)
    {
        $this->articuloID = $articuloID;

        return $this;
    }
 
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getPrecio()
    {
        return $this->precio;
    }

    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    public function getImagen()
    {
        return $this->imagen;
    }

    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getRubroID()
    {
        return $this->rubroID;
    }

    public function setRubroID($rubroID)
    {
        $this->rubroID = $rubroID;

        return $this;
    }
    
}

?>