<?php
include("Views/header.php");
include(VIEWS_PATH."nav.php");
include("Repository/PublicacionRepository.php");
?>
<main class="d-flex align-items-center justify-content-center height-100" >
    <div class="articulo-content">
        <div class="form-content-articulo">
<?php
    $publicacion = GetPublicacion($_POST["publicacionId"])[0];
    $consultas = GetConsultasByPublicacion($_POST["publicacionId"]);

    $resultConsultas = "";

    foreach ($consultas as $consulta){
        $resultConsultas = $resultConsultas."
            <p>".$consulta[0]." - ".$consulta[1]." - ".$consulta[2]."</p><br>";
    }
    
    $img = "";
    if($publicacion[5] != ""){
        $img = "<img src='Views/img/".$publicacion[5]."' class='w3-left w3-circle' width='150' height='150'>";
    }

    $consulta = "";
    if(isset($_SESSION["isLogged"])){
        $consulta = "<input type='text' id='comentario' class='form-control form-control-md login-input' placeholder='Escriba su consulta'>
        <button onclick='comentar();' class='btn btn-primary'>Consultar</button>";
    }

    $result = "
        <input type='hidden' id='publicacionId' value='".$publicacion[0]."'/>
        <div class='w3-card-4' style='padding-bottom:10px'>

            <header class='w3-container w3-light-grey'>
            <h3>".$publicacion[3]."</h3>
            </header>
            
            <div class='w3-container'>
            <p>Precio: ".$publicacion[4]."</p>
            <hr>
            ".$img."
            <p>Rubro: ".$publicacion[6]."</p>
            <p>Publicado desde: ".$publicacion[1]."</p>
            ".$consulta."
            </div>
        
            <br><br>
            <h3>Consultas:</h3>
            <div id='divConsultas'>
                ".$resultConsultas."
            </div>
        </div>
        <hr>
        </form>";
        echo $result;
?>

        </div>
    </div>
</main>
<?php
include(VIEWS_PATH."footer.php");
?>

<script>


    function comentar(){
        if($("#comentario").val() == ""){
            alert("Debe ingresar un comentario.");

            return false;
        }
        var parametros = {
            "comentario": $("#comentario").val(),
            "publicacionId": $("#publicacionId").val()
        };
        $.ajax({
            data:  parametros,
            url:   'comentarPublicacion.php',
            type:  'post',
            beforeSend: function () {
                $("#resultado").html("Procesando, espere por favor...");
            },
            success:  function (response) {
                $("#comentario").val("");
                $("#divConsultas").html(response + $("#divConsultas").html());
            }
        });
    }

</script>