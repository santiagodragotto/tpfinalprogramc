<?php
    include("Views/header.php");
    unset($_SESSION["isLogged"]);
    unset($_SESSION["User"]);

    header("Location: index.php");
    stop;
?>