<?php
    include("Views/header.php");
    include("Repository/UsuarioRepository.php");
    use Models\Usuario as Usuario;

    if($_POST) {
        $result = GetUsuario($_POST["username"], md5($_POST["password"]));
        if(!empty($result)){
            $_SESSION["isLogged"] = true;
            $_SESSION["User"] = $result;
            header("Location: index.php");
            stop;
        } else {
            $_SESSION["inputNombreUsuario"] = $_POST["username"];
            $_SESSION["inputPassword"] = $_POST["password"];
            $_SESSION["message"] = "El nombre de usuario o la contraseña son incorrectas.";
            header("Location: login.php");
            stop;
        }
    } else {
        header("Location: login.php");
        stop;
    }

?>