<?php
    include("Views/header.php");
    include(VIEWS_PATH."nav.php");

    include("Repository/PublicacionRepository.php");
?>
<main class="d-flex align-items-center justify-content-center height-100" >
    <div class="articulo-content">
        <div class="form-content-articulo">
<?php

    $publicaciones = GetPublicacionesByUsuario($_SESSION["User"]["UsuarioID"]);
    $result = "";
    foreach ($publicaciones as $publicacion){
        $img = "";
        if($publicacion[5] != ""){
            $img = "<img src='Views/img/".$publicacion[5]."' class='w3-left w3-circle' width='150' height='150'>";
        }

        $result = $result."
        <form action='".FRONT_ROOT."verpublicacion.php' method='POST' class='login-form'>
        <input type='hidden' name='publicacionId' value='".$publicacion[0]."'/>
        <div class='w3-card-4' style='padding-bottom:10px'>

            <header class='w3-container w3-light-grey'>
            <h3>".$publicacion[3]."</h3>
            </header>
            
            <div class='w3-container'>
            <p>Precio: ".$publicacion[4]."</p>
            <hr>
            ".$img."
            <p>Rubro: ".$publicacion[6]."</p>
            <p>Publicado desde: ".$publicacion[1]."</p>
            </div>
            
            <button type='submit' class='w3-btn-block w3-dark-grey'>+ Ver</button>
        
        </div>
        <hr>
        </form>";
    }
    echo $result;

?>
        </div>
    </div>
</main>

<?php
include(VIEWS_PATH."footer.php");
?>
