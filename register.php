<?php
include("Views/header.php");
include(VIEWS_PATH."nav.php");

?>

<link rel="stylesheet" href="<?= CSS_PATH ?>register.css">
<script src="<?= JS_PATH ?>register.js" crossorigin="anonymous"></script>

<main class="d-flex align-items-center justify-content-center height-100" >
    <div class="login-content">
        <div class="login-logo text-center">
                <img src="<?= IMG_PATH ?>logocarro.png" alt="Productos Logo" width="114px" height="116px"/>
                <h2 id="login-title">
                <?php
                if(isset($_SESSION["inputUsuarioId"])){
                    echo "Mi Perfil";
                } else {
                    echo "Registro de usuario";
                }
                ?>
                </h2>
        </div>
        
        <form action="<?= FRONT_ROOT ?>registerpost.php" method="POST" class="login-form">
                <!-- Error en registro -->
                <?php
                    if(isset($_SESSION["errors"])){
                        foreach($_SESSION["errors"] as $error){
                            echo "<strong style='color:red;'>$error</strong><br/>";
                        }
                    }
                    unset($_SESSION["errors"]);
                ?>

                <div class="form-content">
                    <input type="hidden" id="usuarioId" name="usuarioId"
                        <?php
                            if(isset($_SESSION["inputUsuarioId"])){
                                echo "value='".$_SESSION["inputUsuarioId"]."'";
                            }
                        ?>
                    >
                    <div class="form-group">
                        <i class="fas fa-pen form-icon"></i>
                        <input type="text" name="apellido" class="form-control form-control-md login-input" placeholder="Apellido" required
                        <?php
                            if(isset($_SESSION["inputApellido"])){
                                echo "value='".$_SESSION["inputApellido"]."'";
                            }
                            unset($_SESSION["inputApellido"]);
                        ?>
                        >
                    </div>
                    <div class="form-group">
                        <i class="fas fa-pen form-icon"></i>
                        <input type="text" name="nombre" class="form-control form-control-md login-input" placeholder="Nombre" required
                        <?php
                            if(isset($_SESSION["inputNombre"])){
                                echo "value='".$_SESSION["inputNombre"]."'";
                            }
                            unset($_SESSION["inputNombre"]);
                        ?>>
                    </div>
                    <div class="form-group">
                        <i class="fas fa-envelope form-icon"></i>
                        <input type="email" name="email" class="form-control form-control-md login-input" placeholder="Email" required
                        <?php
                            if(isset($_SESSION["inputEmail"])){
                                echo "value='".$_SESSION["inputEmail"]."'";
                            }
                            unset($_SESSION["inputEmail"]);
                        ?>
                        >
                    </div>
                    <div class="form-group">
                        <i class="fas fa-user form-icon"></i>
                        <input type="text" name="username" class="form-control form-control-md login-input" placeholder="Nombre de Usuario" required
                        <?php
                            if(isset($_SESSION["inputNombreUsuario"])){
                                echo "value='".$_SESSION["inputNombreUsuario"]."'";
                            }
                            unset($_SESSION["inputNombreUsuario"]);
                        ?>
                        >
                    </div>
                    <div class="form-group">
                        <i class="fas fa-lock form-icon"></i>
                        <input type="password" id="password" name="password" class="form-control form-control-md login-input" placeholder="Contraseña" required
                        <?php
                            if(isset($_SESSION["inputPassword"])){
                                echo "value='".$_SESSION["inputPassword"]."'";
                            }
                            unset($_SESSION["inputPassword"]);
                        ?>
                        >
                    </div>
                    <div class="form-group">
                        <i class="fas fa-lock form-icon"></i>
                        <input type="password" id="confirmpassword" name="confirmpassword" class="form-control form-control-md login-input" placeholder="Confirmar Contraseña" required
                        <?php
                            if(isset($_SESSION["inputConfirmPassword"])){
                                echo "value='".$_SESSION["inputConfirmPassword"]."'";
                            }
                            unset($_SESSION["inputConfirmPassword"]);
                        ?>
                        >
                    </div>
                    <div class="form-group">
                        <label class="login-input-label" for="sexo" style="width:100%">Genero</label>
                        <label class="label" style="padding-right:5px">
                            <input type="radio" name="sexo" value="F"
                            <?php
                                if(isset($_SESSION["inputSexo"]) && $_SESSION["inputSexo"] == "F"){
                                    echo "checked";
                                }
                            ?>
                            > Femenino
                            <span class="checkmark"></span>
                        </label>    
                        <label class="label" style="padding-right:5px">
                            <input type="radio" name="sexo" value="M"
                            <?php
                                if(isset($_SESSION["inputSexo"]) && $_SESSION["inputSexo"] == "M"){
                                    echo "checked";
                                }
                            ?>
                            > Masculino
                            <span class="checkmark"></span>
                        </label>
                        <label class="label">
                            <input type="radio" name="sexo" value="O"
                            <?php
                                if(isset($_SESSION["inputSexo"]) && $_SESSION["inputSexo"] == "O"){
                                    echo "checked";
                                }
                                unset($_SESSION["inputSexo"]);
                            ?>
                            > Otro
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="form-group">
                        <i class="fas fa-calendar-day form-icon"></i>
                        <input type="date" class="form-control form-control-md login-input" placeholder="Cumpleaños" name="fechaNacimiento"
                        <?php
                            if(isset($_SESSION["inputFechaNacimiento"])){
                                echo "value='".$_SESSION["inputFechaNacimiento"]."'";
                            }
                            unset($_SESSION["inputFechaNacimiento"]);
                        ?>
                        >
                    </div>
                    <div class="form-group">
                        <i class="fas fa-phone form-icon"></i>
                        <input type="tel" name="telefono" class="form-control form-control-md login-input" placeholder="Teléfono" pattern="[0-9]{10}"
                        <?php
                            if(isset($_SESSION["inputTelefono"])){
                                echo "value='".$_SESSION["inputTelefono"]."'";
                            }
                            unset($_SESSION["inputTelefono"]);
                        ?>
                        >
                    </div>  
                </div>
                <button class="login-btn" type="submit" onclick="validarUsuario(event);">
                <?php
                    if(isset($_SESSION["inputUsuarioId"])){
                        echo "Guardar";
                    } else {
                        echo "Registrarse";
                    }
                    unset($_SESSION["inputUsuarioId"]);
                ?>
                </button>
        </form>
    </div>
</main>

<?php
include(VIEWS_PATH."footer.php");
?>